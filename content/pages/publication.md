Title: Autres articles
Date: 2019/07/11
Slug: autres-publications
Authors: Trouverie Joachim

# Mes autres publications

Dans le cadre de mon travail je suis amené à écrire d'autres publications.
J'ai regroupé ci-dessous les liens vers les articles que j'ai pu rédigé

* [Créer et déployer un package Python avec Poetry et Pypiserver](https://www.anybox.fr/blog/creer-et-deployer-un-package-python/)
