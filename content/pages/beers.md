Title: Mes bières
Author: Trouverie Joachim
Date: 27/04/2019
save_as: category/mes-bieres.html

# Mes recettes de bières

J'ai commencé à brasser de la bière en 2017 avec de l'extrait de malt. Après un
premier brassin j'ai vite sauté le pas et j'ai commencé à brasser en tout grain.
Je glâne ici et là mes recettes et les personnalise éventuellement. Voici une
sélection de mes recettes testées.

Pour trouver des recettes je peux vous conseiller d'aller faire un tour sur:

* [Le forum brassage amateur](http://www.brassageamateur.com/forum/)
* [Univers Bière](http://univers-biere.net)

Elles sont rangées par catégories et styles.

Toutes mes recettes sont stockées au format [beeryaml](https://beeryaml.readthedocs.io/)
et dispose d'un lien de téléchargement, vous pouvez ensuite les convertir en
format [beerxml](http://www.beerxml.com/beerxml.htm) afin de les importer dans
le logiciel Beersmith par exemple.

Vous pourrez remarquer que le nom de chacune correspond à un méchant de Batman
avec plus ou moins de rapport selon le style de la bière.

* **Stout**
    * [Catwoman]({filename}/catwoman.byml)

* **Belge double**
    * [Anarky]({filename}/anarky.byml)

* **Belge Triple**
    * [Bane]({filename}/bane.byml)

* **Indian Pale Ale**
    * [Poison Ivy]({filename}/ivy.byml)
