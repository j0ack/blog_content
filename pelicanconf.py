#!/usr/bin/env python
# -*- coding: utf-8 -*- #

from __future__ import unicode_literals
import os

AUTHOR = "TROUVERIE Joachim"
SITENAME = "Joack's Blog"
SITEURL = ""
PATH = "content"
TIMEZONE = "Europe/Paris"
DEFAULT_LANG = "fr"

# Feed generation is usually not desired when developing
FEED_ALL_ATOM = None
CATEGORY_FEED_ATOM = None
TRANSLATION_FEED_ATOM = None
AUTHOR_FEED_ATOM = None
AUTHOR_FEED_RSS = None

THEME = os.path.join(".", "themes", "j0ack")

# Social widget
SOCIAL = (
    ("github", "https://github.com/j0ack/"),
    ("twitter", "https://twitter.com/j0ack_/"),
    ("linkedin", "https://www.linkedin.com/in/joachim-trouverie-60842052/"),
)

DEFAULT_PAGINATION = 5

AVATAR = "https://avatars0.githubusercontent.com/u/9861290?s=460&v=4"
SIDEBAR_DIGEST = "Linux tricks, Python, Docker and Beers"
DISPLAY_CATEGORIES_ON_MENU = False
MENUITEMS = (
    ("Accueil", SITEURL),
    ("Python", f"{SITEURL}/category/python.html"),
)

# Uncomment following line if you want document-relative URLs when developing
# RELATIVE_URLS = True

PLUGINS = ["beer_reader"]
