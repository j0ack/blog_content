#!/usr/bin/env python3

import os

from jinja2 import Template
from pelican import signals
from pelican.readers import BaseReader
from pybeeryaml import Recipe

TEMPLATE = """
<section class="h-recipe">
  {% if recipe.notes %}
  <p>{{ recipe.notes }}</p>
  {% endif %}
  <span class="p-author">{{ recipe.brewer }}</span>
  <div class="info-ctn">
    <img class="label" src="{static}/image/{{ recipe.name.lower() }}.png" />
    <div>
      <ol>
        <li>{{ recipe.type }}</li>
        <li>Batch size : {{ recipe.batch_size }}</li>
        <li>Boil size : {{ recipe.boil_size }}</li>
        <li>Boil time : {{ recipe.boil_time }}</li>
        <li>Style : {{ recipe.style }}</li>
        <li><a href="{attach}{{ filename }}">Télécharger la recette</a></li>
      </ol>
    </div>
  </div>
  <h2 class="header">Fermentables</h2>
  <ul>
    {% for fermentable in recipe.fermentables %}
    <li class="p-ingredient">
      {{ fermentable.name }}
      {{ fermentable.color }}
      <span class="s-amount">{{ fermentable.amount }}</span>
    </li>
    {% endfor %}
  </ul>
  <h2 class="header">Hops</h2>
  <ul>
    {% for hop in recipe.hops %}
    <li class="p-ingredient">
      {{ hop.name}}
      {{ hop.alpha }}
      [ {{ hop.use }}({{ hop.time }}) ]
      <span class="s-amount">{{ hop.amount }}</span>
    </li>
    {% endfor %}
  </ul>
  <h2 class="header">Yeasts</h2>
  <ul>
    {% for yeast in recipe.yeasts %}
    <li class="p-ingredient">
      {{ yeast.name }}
      {{ yeast.form }}
      <span class="s-amount">{{ yeast.amount }}</span>
    </li>
    {% endfor %}
  </ul>
  <h2 class="header">{{ recipe.mash.name }}</h2>
  <ul>
    {% for step in recipe.mash.mash_steps %}
    <li class="p-ingredient">
      {{ step.name }}
      <span class="s-time">{{ step.step_time }}</span>
      <span class="s-temp">{{ step.step_temp }}</span>
    </li>
    {% endfor %}
  </ul>
  {% if recipe.primary_age %}
  <h2 class="header">Fermentation</h2>
  <div class="fermentation">
    <ul>
      <li class="p-ingredient">
        <span class="s-time">{{ recipe.primary_age }}</span>
        <span class="s-temp">{{ recipe.primary_temp }}</span>
      </li>
      {% if recipe.secondary_age %}
      <li class="p-ingredient">
        <span class="s-time">{{ recipe.secondary_age }}</span>
        <span class="s-temp">{{ recipe.secondary_temp }}</span>
      </li>
      {% endif %}
      {% if recipe.tertiary_age %}
      <li class="p-ingredient">
        <span class="s-time">{{ recipe.tertiary_age }}</span>
        <span class="s-temp">{{ recipe.tertiary_temp }}</span>
      </li>
      {% endif %}
    </ul>
  </div>
  {% endif %}
</section>
"""


class BeerReader(BaseReader):

    enabled = True
    file_extensions = ["byml", "yml"]

    def read(self, filename):
        recipe = Recipe.from_file(filename)
        metadata = {
            "title": recipe.name,
            "category": "Mes bières",
            "date": recipe.date,
            "summary": recipe.notes if hasattr(recipe, "notes") else "",
            "cover_image": "image/{}.png".format(recipe.name.lower()),
        }

        parsed = {}
        for key, value in metadata.items():
            parsed[key] = self.process_metadata(key, value)

        template = Template(TEMPLATE)
        content = template.render(
            recipe=recipe, filename=os.path.basename(filename)
        )

        return content, parsed


def add_reader(readers):
    readers.reader_classes["byml"] = BeerReader


def register():
    signals.readers_init.connect(add_reader)
